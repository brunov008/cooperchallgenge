import { BaseResponse } from "./base.response";

export interface UserFindOneResponse{
    id: number;
    email: string;
    cpf: string;
    username: string;
    permissions: string[];
    role: string;
}