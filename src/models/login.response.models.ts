import { BaseResponse } from "./base.response";

export interface LoginResponse extends BaseResponse{
  id: number;
  email: string;
  username: string;
  permissions: string[];
  role: string;
}