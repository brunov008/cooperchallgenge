import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class LoginRequest {

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly user: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly password: string;
}