import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString, IsArray } from "class-validator";

export class UserPermissions{

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly idUser: number;
   
  @IsString()
  @IsNotEmpty() //FIXME opcional
  @ApiProperty()
  readonly role: string;

  @IsArray()
  @IsNotEmpty() //FIXME opcional
  @ApiProperty()
  readonly permissions: string[];
}