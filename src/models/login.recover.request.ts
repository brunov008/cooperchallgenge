import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class LoginRecoverRequest {

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  user: string;

  token?: string;
}