import { LoginRecoverResponse } from './login.recover.response';

export interface LoginRecoverPart2Response extends LoginRecoverResponse{
  token: string;
}