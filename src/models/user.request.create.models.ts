import { IsNotEmpty, IsString} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UserCreateRequest {

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    user: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly password: string;
}