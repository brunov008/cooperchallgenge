import { IsNotEmpty, IsArray } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { LoginRequest } from './login.request.models';
import { UserPermissions } from './user.permission.request';

export class LoginUserPermissions extends LoginRequest {

  @IsNotEmpty()  
  @IsArray()
  @ApiProperty()
  readonly users: UserPermissions[];

}