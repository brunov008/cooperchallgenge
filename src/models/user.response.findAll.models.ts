import { UserFindOneResponse } from "./user.response.find";
import { BaseResponse } from "./base.response";

export interface UserFindAllResponse extends BaseResponse{
    result: UserFindOneResponse[];
}