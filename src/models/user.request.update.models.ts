import { IsNotEmpty, IsString,  } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UserUpdateRequest {

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly cpf: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly email: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly username: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly password: string;
}