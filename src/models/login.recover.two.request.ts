import { IsNotEmpty, IsString } from 'class-validator';
import { LoginRecoverRequest } from './login.recover.request';

export class LoginRecoverPart2Request extends LoginRecoverRequest{

  @IsString()
  @IsNotEmpty()
  password: string;

  @IsString()
  @IsNotEmpty()
  confirmPassword: string;
}