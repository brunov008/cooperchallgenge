import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  
  const appOptions = {cors: true};
  const app = await NestFactory.create(AppModule, appOptions);
  app.useGlobalPipes(new ValidationPipe({
    disableErrorMessages: false,
    transform: true
  }));
  app.setGlobalPrefix('api');

  const config = new DocumentBuilder()
    .setTitle('Cooper example')
    .setDescription('The cooper API - http://localhost')
    .setContact('Bruno Oliveira', '', 'brunomarcio2006@gmail.com')
    .setVersion('1.0.0')
    .build();
  
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('docs', app, document);

  await app.listen(80);
}
bootstrap();
