import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { UserModule } from './user/user.module';
import { LoginModule } from './login/login.module';
import { PermissionModule } from './permissions/permissions.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    UserModule,
    LoginModule,
    PermissionModule
  ],
  providers: [], 
})
export class AppModule {
  constructor(private readonly connection: Connection) {
  }
}
