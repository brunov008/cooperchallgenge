export class Utils {
    static generateCode = () : string => {
        return Math.floor(100000 + Math.random() * 900000).toString()
    }

    static isEmail = (value : string) : boolean=> {
        return (value.includes('@') && value.includes('.com'))
    }

    static isCpf = (value: string) : boolean=> {
        const pattern : any = /([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})/
        
        return (value.match(pattern) === null) ? false : true

    }

    static cleanCpf = (value : string) : string => {
        return value.replace('.', '').replace('-', '')
    }

    static randomIntFromInterval = (min: number, max: number) : number=> {
        return Math.floor(Math.random() * (max - min + 1) + min);
      }
}