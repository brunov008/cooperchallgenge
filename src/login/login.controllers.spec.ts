import { Test } from '@nestjs/testing';
import { LoginController } from './login.controller';
import { UserService } from '../user/user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RolesEntity } from '../entities/roles.entity';
import { UserRolerEntity } from '../entities/userRole.entity';
import { UserEntity } from '../entities/user.entity';
import { PermissionsEntity } from '../entities/permissions.entity';
import { UserPermissionsEntity } from '../entities/userPermissions.entity';
import { UserCreateRequest } from '../models/user.request.create.models';
import { BaseResponse } from '../models/base.response';
import { MockService } from '../shared/mock.service.shared';
import { INestApplication, HttpException } from '@nestjs/common';
import { Utils } from '../util/utility.util';
import { LoginRequest } from 'src/models/login.request.models';
import { LoginResponse } from 'src/models/login.response.models';
import { LoginRecoverRequest } from 'src/models/login.recover.request';
import { LoginRecoverPart2Response } from 'src/models/login.recover.two.response';


describe('LoginController', () => {
  let app: INestApplication;
  let loginController: LoginController;
  
  //ANTES
  beforeAll(async () => {
    const module = await Test.createTestingModule({ 
        imports: [
            TypeOrmModule.forRoot(),
            TypeOrmModule.forFeature([RolesEntity, UserRolerEntity, UserEntity, PermissionsEntity, UserPermissionsEntity, PermissionsEntity]), 
        ],
        controllers: [LoginController],
        providers: [UserService],
      })
      //.overrideProvider(UserService) 
      //.useClass(MockService)
      .compile();

      loginController = module.get<LoginController>(LoginController);

      app = module.createNestApplication();
      await app.init();
  });

  //DEPOIS
  afterAll(async () => await app.close());
   
  it('Deve criar um usuario utilizando um email válido', async () => {

      const randomNumber = Utils.randomIntFromInterval(1, 10000)

      const user : UserCreateRequest= {
          user: `teste${randomNumber}@teste.com`,
          password: '123'
      }

      const res = await loginController.create(user)

      const response : BaseResponse = { message: 'Usuário criado com sucesso.' }
      
      expect(JSON.stringify(res)).toBe(JSON.stringify(response));
  })

  it('Nao deve criar um usuario utilizando um email inválido', async () => {

    const user : UserCreateRequest= {
        user: `1##4`,
        password: '123'
    }

    loginController.create(user)
    .catch((e:HttpException) => {
      expect(e.getStatus()).toBe(400)
    })
  })

  it('Deve logar com o usuario seed e conter todas as permissoes', async () => {

    const user : LoginRequest = {
        user: `seed`,
        password: 'admin123'
    }

    const res = await loginController.login(user)

    expect(res.permissions.length).toBeGreaterThanOrEqual(4);
  })

  it('Nao deve logar com usuario inexistente', async () => {

    const request : LoginRequest = {
        user: `jjj@ttt.com`,
        password: '123'
    }

    loginController.login(request)
    .catch((error: HttpException)=> {
      expect(error.getStatus()).toBe(404)
    })
  })

  it('Deve retornar token para recuperação de senha', async () => {

    const res = await loginController.recover('seed@cooper.com')

    expect(res.token).not.toBeNull()
  })

  it('Deve retornar error de validacao', async () => {

    loginController.recover('seed111ççç0')
    .catch((error: HttpException) => {
      expect(error.getStatus()).toBe(400)
    })

  })
})

