import {Module, } from '@nestjs/common';
import { LoginController } from './login.controller';
import { UserService } from '../user/user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRolerEntity } from '../entities/userRole.entity';
import { UserEntity } from '../entities/user.entity';
import { PermissionsEntity } from '../entities/permissions.entity';
import { UserPermissionsEntity } from '../entities/userPermissions.entity';
import { RolesEntity } from '../entities/roles.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([RolesEntity, UserRolerEntity, UserEntity, PermissionsEntity, UserPermissionsEntity, PermissionsEntity]), 
  ],
  controllers: [LoginController],
  providers: [UserService],
})
export class LoginModule{
   
}
