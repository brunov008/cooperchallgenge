import { Get, Post, Body, Param, Controller, HttpException, Query} from '@nestjs/common';
import { LoginRequest } from '../models/login.request.models';
import { LoginResponse } from '../models/login.response.models';
import { LoginRecoverRequest } from '../models/login.recover.request';
import { LoginRecoverPart2Request } from '../models/login.recover.two.request';
import { LoginRecoverPart2Response } from '../models/login.recover.two.response';
import { LoginRecoverResponse } from '../models/login.recover.response';
import { UserService } from '../user/user.service';
import { UserCreateRequest } from '../models/user.request.create.models';
import { BaseResponse } from '../models/base.response';
import { Utils } from '../util/utility.util';
import { ApiOperation, ApiResponse, ApiParam, ApiQuery } from '@nestjs/swagger';
import { query } from 'express';

@Controller('login')
export class LoginController {
    constructor(private userService: UserService) {}

    @ApiOperation({ summary: 'Realizar login' })
    @ApiResponse({ status: 201, description: 'Retorna o usuário cadastrado.'})
    @ApiResponse({ status: 400, description: 'Retorna uma mensagem contendo erro de validação'})
    @ApiResponse({ status: 404, description: 'Retorna uma mensagem de usuário nao cadastrado'})
    @Post()
    async login(@Body() loginUser: LoginRequest): Promise<LoginResponse> { 
        return await this.userService.findOne(loginUser)
    }

    @ApiOperation({ summary: 'Criar usuário' })
    @ApiResponse({ status: 201, description: 'Retorna uma mensagem de sucesso'})
    @ApiResponse({ status: 400, description: 'Retorna uma mensagem contendo erro de validação'})
    @ApiResponse({ status: 422, description: 'Retorna uma mensagem de usuário ja cadastrado'})
    @ApiResponse({ status: 500, description: 'Erro inesperado'})
    @Post('create')
    async create(@Body() body : UserCreateRequest) : Promise<BaseResponse>{
        await this.userService.create(body) 

        return { message: 'Usuário criado com sucesso.' }
    }

    @ApiOperation({ summary: 'Recuperar senha de um usuário' })
    @ApiResponse({ status: 201, description: 'Retorna uma mensagem de sucesso e o token necessário para trocar a senha'})
    @ApiResponse({ status: 400, description: 'Retorna uma mensagem contendo erro de validação'})
    @ApiResponse({ status: 401, description: 'Usuário nao encontrado'})
    @ApiParam({ name: 'user', type: String, description:'Deve ser passado o email ou cpf como parâmetro', example: '/login/recover/teste@gmail.com'})
    @Get('recover/:user')
    async recover(@Param('user') user: string): Promise<LoginRecoverPart2Response> {

        //:D
        if(!(Utils.isCpf(user) || Utils.isEmail(user))) throw new HttpException({message: 'O campo user deve conter um cpf ou email válido.'}, 400)

        const res = {
            user : user,
            token : null
        }

        const _user = await this.userService.recoverByEmailOrCpf(res)

        const {id_user} = _user

        const token = await this.userService.updateToken(id_user)

        return {
            token: token, //retirar em producao (utilizado apenas para fins de demonstracao)
            message: `Um email foi enviado para ${id_user.email} contendo o token necessário para trocar sua senha.`
        }
    }

    @ApiOperation({ summary: 'Utilizar o token recebido da rota /recover para trocar a senha' })
    @ApiResponse({ status: 201, description: 'Retorna uma mensagem de sucesso'})
    @ApiResponse({ status: 400, description: 'Retorna uma mensagem contendo erro de validação'})
    @ApiResponse({ status: 401, description: 'Usuário nao encontrado'})
    @ApiParam({ name: 'user', type: String, description:'Deve ser passado o email ou cpf como parâmetro', example: '/login/recover/teste@gmail.com/2?senha=123&confirmaSenha=123&token=<Token recebido>'})
    @ApiQuery({name:'senha'})
    @ApiQuery({name:'confirmaSenha'})
    @ApiQuery({name:'token'})
    @Get('recover/:user/2')
    async recoverPart2(@Param('user') user: string, @Query() query): Promise<LoginRecoverResponse> {

        if(!(Utils.isCpf(user) || Utils.isEmail(user))) throw new HttpException({message: 'O campo user deve conter um cpf ou email válido.'}, 400)

        const {senha, confirmaSenha, token} = query

        //FIXME criar pipe de validacao
        const dataInput = new LoginRecoverPart2Request()
        dataInput.user = user
        dataInput.password = senha
        dataInput.confirmPassword = confirmaSenha
        dataInput.token = token

        if(dataInput.token == null) throw new HttpException({message: 'O campo token é obrigatório'}, 400)
        if(dataInput.password == null) throw new HttpException({message: 'O campo senha é obrigatório'}, 400)
        if(dataInput.confirmPassword == null) throw new HttpException({message: 'O campo confirmaSenha é obrigatório'}, 400)

        if(dataInput.password !== dataInput.confirmPassword) throw new HttpException({message: 'As senhas devem ser iguais'}, 400)

        const _user = await this.userService.recoverByEmailOrCpf(dataInput)

        await this.userService.updatePassword(dataInput.password, _user.id_user)

        return { message:  'Senha atualizada com sucesso!' }
    }
} 