import { Body, Put, Param, Controller, Post} from '@nestjs/common';
import { BaseResponse } from '../models/base.response';
import { ApiOperation, ApiResponse, ApiBody } from '@nestjs/swagger';
import { PermissionService } from './permissions.service';
import { LoginRequest } from '../models/login.request.models';
import { LoginUserPermissions } from '../models/login.seed.permissions.request';

@Controller('permissions')
export class PermissionsController {
    constructor(private permissionService: PermissionService) {}

    @ApiOperation({ summary: 'Acessar rota apenas com administrador e permissao de cadastrar'})
    @ApiResponse({ status: 201, description: 'Retorna uma mensagem de sucesso.'})
    @ApiResponse({ status: 400, description: 'Retorna uma mensagem contendo erro de validação'})
    @ApiResponse({ status: 404})
    @ApiResponse({ status: 401, description: 'Acesso negado (rota privada)'})
    @ApiResponse({ status: 500, description: 'Erro no servidor'})
    @Post()
    async teste(@Body() loginUser: LoginRequest) : Promise<BaseResponse>{
        await this.permissionService.findOne(loginUser)

        return { status: true }
    }

    @ApiOperation({ summary: 'Atualizar permissoes e perfis de usuarios atraves do usuario seed (Master)' })
    @ApiResponse({ status: 201, description: 'Retorna uma mensagem de sucesso.'})
    @ApiResponse({ status: 400, description: 'Retorna uma mensagem contendo erro de validação'})
    @ApiResponse({ status: 500, description: 'Erro no servidor'})
    @Put()
    async changePermissions(@Body() userData: LoginUserPermissions) : Promise<BaseResponse>{
        await this.permissionService.updatePermissionsAndRole(userData)

        return { message: 'Permissoes e roles atualizados com sucesso.' }
    }
} 