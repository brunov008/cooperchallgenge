import {Module, } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRolerEntity } from '../entities/userRole.entity';
import { UserEntity } from '../entities/user.entity';
import { PermissionsEntity } from '../entities/permissions.entity';
import { UserPermissionsEntity } from '../entities/userPermissions.entity';
import { RolesEntity } from '../entities/roles.entity';
import { PermissionsController } from './permissions.controller';
import { PermissionService } from './permissions.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([RolesEntity, UserRolerEntity, UserEntity, PermissionsEntity, UserPermissionsEntity, PermissionsEntity]), 
  ],
  controllers: [PermissionsController],
  providers: [PermissionService],
})
export class PermissionModule{
   
}
