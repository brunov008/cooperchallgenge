import { Injectable, HttpException } from '@nestjs/common';
import { InjectRepository, InjectConnection } from '@nestjs/typeorm';
import { Repository, QueryFailedError, Connection } from 'typeorm';
import { LoginRequest } from '../models/login.request.models';
import { UserEntity } from '../entities/user.entity';
import { UserRolerEntity } from '../entities/userRole.entity';
import { LoginRecoverRequest } from '../models/login.recover.request';
import { LoginRecoverPart2Request } from '../models/login.recover.two.request';
import { Utils } from '../util/utility.util'
import { UserCreateRequest } from '../models/user.request.create.models';
import { UserUpdateRequest } from '../models/user.request.update.models';
import { PermissionsEntity } from '../entities/permissions.entity';
import { UserPermissionsEntity } from '../entities/userPermissions.entity';
import { UserFindAllResponse } from '../models/user.response.findAll.models';
import { RolesEntity } from '../entities/roles.entity';
import { LoginResponse } from '../models/login.response.models';
import { UserFindOneResponse } from '../models/user.response.find';
import { LoginUserPermissions } from '../models/login.seed.permissions.request';
import {UNAUTHORIZED} from 'http-status'

const sha256 = require('sha256')

@Injectable()
export class PermissionService{

    constructor(
        @InjectConnection() private connection: Connection,

        @InjectRepository(UserRolerEntity)
        private readonly userRoleRepository: Repository<UserRolerEntity>,

        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>,

        @InjectRepository(PermissionsEntity)
        private readonly permissionsRepository: Repository<PermissionsEntity>,

        @InjectRepository(UserPermissionsEntity)
        private readonly userPermissionsRepository: Repository<UserPermissionsEntity>,

        @InjectRepository(RolesEntity)
        private readonly rolesRepository: Repository<RolesEntity>
      ) {}

      async findOne({user, password} : LoginRequest): Promise<void>{

        const _result = await this.userExistByUserAndPassword(user, password)

        const {id_user, id_role} = _result

        const _permissionsResult = await this.userPermissionsRepository
        .createQueryBuilder('usr')
        .innerJoin('usr.id_user', 'id_user')
        .addSelect(['usr.permissions', 'id_user.id'])
        .where('id_user.id = :id', {id: id_user.id})
        .getOne()

        const isAuthorized = (id_role.role === 'ADMIN' && _permissionsResult.permissions.some(item => item === 'Cadastrar'))

        if(!isAuthorized) throw new HttpException({message: 'Apenas administradores que possuem permissão de cadastrar podem acessar'}, UNAUTHORIZED)
      }

      async userExistById(id : number) : Promise<UserRolerEntity>{
        const _result = await this.userRoleRepository
        .createQueryBuilder('usr')
        .innerJoin('usr.id_user', 'id_user')
        .innerJoin('usr.id_role', 'id_role')
        .addSelect(['id_user.id', 'id_user.email', 'id_user.created_on', 'id_user.username', 'id_role.role', 'id_user.master'])
        .where(qb => {
          qb.where('id_user.id = :id', {id: id})
        })
        .getOne()

        if(!_result) throw new HttpException({message: 'Usuario nao cadastrado'}, 404)

        return _result
      }

      //FIXME remodelar tabela user_permissions e user_roles
      async updatePermissionsAndRole({user, password, users}: LoginUserPermissions) : Promise<void>{
        const _result = await this.userExistByUserAndPassword(user, password)

        const {id_user} = _result

        if(id_user.master !== '1') throw new HttpException({message: 'Você nao possui acesso a esse conteudo'}, UNAUTHORIZED)

        for await (const item of users){

          const _userToFind = await this.userExistById(item.idUser)

          if(_userToFind){
            const _roleResult = await this.rolesRepository.findOne({ role: item.role})

            if(_roleResult){
              const _permissionsResult = await this.userPermissionsRepository
              .createQueryBuilder('usr')
              .innerJoin('usr.id_user', 'id_user')
              .addSelect(['usr.permissions'])
              .where('id_user.id = :id', {id: _userToFind.id_user.id})
              .getOne()

              if(_permissionsResult){
                const _permissionResultUpdate = await this.userPermissionsRepository.createQueryBuilder()
                  .update(_permissionsResult)
                  .set({
                    permissions: item.permissions
                  })
                  .where('id = :id', {id: _permissionsResult.id})
                  .execute()

                if(_permissionResultUpdate.affected > 0){
                  
                  const _resultUserRolesUpdate = await this.userRoleRepository.createQueryBuilder()
                  .update(_userToFind)
                  .set({
                    id_role: _roleResult,
                  })
                  .where('id = :id', {id: _userToFind.id})
                  .execute()

                  if(_resultUserRolesUpdate.affected < 1){}
                }else{
                  console.log('error')
                }
              }
            }
          }
        }
    }

      async userExistByUserAndPassword(user : string, password: string) : Promise<UserRolerEntity>{
        const _result = await this.userRoleRepository
        .createQueryBuilder('usr')
        .innerJoin('usr.id_user', 'id_user')
        .innerJoin('usr.id_role', 'id_role')
        .addSelect(['id_user.id', 'id_user.email', 'id_user.created_on', 'id_user.username', 'id_role.role', 'id_user.master'])
        .where(qb => {
          if(Utils.isCpf(user)){
            qb.where('id_user.cpf = :cpf', {cpf: Utils.cleanCpf(user)}).andWhere('id_user.password = :passwordX', {passwordX: sha256(password)})
          }else if(Utils.isEmail(user)){
            qb.where('id_user.email = :email', {email: user}).andWhere('id_user.password = :passwordY', {passwordY: sha256(password)})
          }
          qb.orWhere('id_user.username = :username', {username: user}).andWhere('id_user.password = :passwordY', {passwordY: sha256(password)})
        })
        .getOne()

        if(!_result) throw new HttpException({message: 'Usuario nao cadastrado'}, 404)

        return _result
      }
}