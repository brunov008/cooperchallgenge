import {Entity, Column, PrimaryColumn} from 'typeorm';

@Entity({ name: "permissions" })
export class PermissionsEntity {

    @PrimaryColumn("character varying", { length: 50 , nullable: false})
    permission: string;

    @Column({type: "timestamp with time zone", default: () => "CURRENT_TIMESTAMP"})
    created_on: string;
}