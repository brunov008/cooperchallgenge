import {Entity, PrimaryGeneratedColumn, Column, OneToMany, Index} from 'typeorm';
import { UserRolerEntity } from './userRole.entity';

@Entity({ name: "roles" })
export class RolesEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("character varying", { length: 9 , nullable: false})
    @Index({ unique: true })
    role: string;

    @Column({type: "timestamp with time zone", default: () => "CURRENT_TIMESTAMP"})
    created_on: string;

    @OneToMany(() => UserRolerEntity, userRole => userRole.id_role)
    user: UserRolerEntity[]
}