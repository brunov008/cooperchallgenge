import {Entity, PrimaryGeneratedColumn, Column, JoinTable, OneToMany, ManyToOne, JoinColumn} from 'typeorm';
import { RolesEntity } from './roles.entity';
import { UserEntity } from './user.entity';

@Entity({ name: "user_roler" })
export class UserRolerEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(()=> RolesEntity, role => role.user)
    @JoinColumn({ name: "id_role" })
    id_role: RolesEntity;
    
    @ManyToOne(()=> UserEntity, user => user.user_role)
    @JoinColumn({ name: "id_user" })
    id_user: UserEntity;
}