import {Entity, PrimaryGeneratedColumn, Column, OneToMany, Index} from 'typeorm';
import { UserRolerEntity } from './userRole.entity';
import { UserPermissionsEntity } from './userPermissions.entity';

@Entity({ name: "users" })
export class UserEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("character varying", { length: 11})
    @Index({ unique: true })
    cpf: string;
    
    @Column("character varying", { length: 100})
    @Index({ unique: true })
    email: string;

    @Column("text")
    token_pass: string;

    @Column({type: "timestamp with time zone", default: () => "CURRENT_TIMESTAMP"})
    created_on: string;

    @Column({type: "timestamp with time zone", default: () => "CURRENT_TIMESTAMP"})
    last_login: string;

    @Column("character varying", { length: 50 })
    @Index({ unique: true })
    username: string;

    @Column("character varying", { length: 254, nullable: false })
    password: string;

    @Column("character")
    master: string;

    @OneToMany(() => UserPermissionsEntity, user => user.id_user)
    user_permissions: UserPermissionsEntity[]

    @OneToMany(() => UserRolerEntity, userRole => userRole.id_user)
    user_role: UserRolerEntity[]
}