import {Entity, PrimaryGeneratedColumn, Column, OneToMany, Index, ManyToOne, JoinColumn} from 'typeorm';
import { UserEntity } from './user.entity';

@Entity({ name: "user_permissions" })
export class UserPermissionsEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({type: "character varying", length: 50 , default: () => "{Visualizar}"})
    permissions: string[];

    @ManyToOne(()=> UserEntity, user => user.user_permissions)
    @JoinColumn({ name: "id_user" })
    id_user: UserEntity;
}