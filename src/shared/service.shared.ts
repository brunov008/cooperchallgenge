import { UserRolerEntity } from "../entities/userRole.entity";
import { LoginRequest } from "../models/login.request.models";
import { LoginRecoverRequest } from "../models/login.recover.request";
import { LoginRecoverPart2Request } from "../models/login.recover.two.request";
import { UserEntity } from "../entities/user.entity";
import { UserCreateRequest } from "../models/user.request.create.models";
import { UserFindAllResponse } from "../models/user.response.findAll.models";
import { LoginResponse } from "../models/login.response.models";
import { UserUpdateRequest } from "../models/user.request.update.models";

export interface BaseService{
    findOne(user: LoginRequest) : Promise<LoginResponse>;
    recoverByEmailOrCpf(user: LoginRecoverRequest| LoginRecoverPart2Request) : Promise<UserRolerEntity>
    updateToken(entity: UserEntity) : Promise<string>
    update(id: number, user : UserUpdateRequest) : Promise<void>
    updatePassword(newPassword: string, entity: UserEntity) : Promise<void>
    create(user: UserCreateRequest) : Promise<void>
    delete(id: number): Promise<void> 
    findAll(): Promise<UserFindAllResponse>
}