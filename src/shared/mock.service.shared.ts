import { BaseService } from './service.shared';
import { LoginResponse } from '../models/login.response.models';
import { LoginRequest } from '../models/login.request.models';
import { LoginRecoverRequest } from '../models/login.recover.request';
import { LoginRecoverPart2Request } from '../models/login.recover.two.request';
import { UserRolerEntity } from '../entities/userRole.entity';
import { UserEntity } from '../entities/user.entity';
import { UserUpdateRequest } from '../models/user.request.update.models';
import { UserCreateRequest } from '../models/user.request.create.models';
import { UserFindAllResponse } from '../models/user.response.findAll.models';

export class MockService implements BaseService{

    async findOne(user: LoginRequest) : Promise<LoginResponse>{
        return null
    }

    async recoverByEmailOrCpf(user: LoginRecoverRequest| LoginRecoverPart2Request) : Promise<UserRolerEntity>{
        return null
    }
    async updateToken(entity: UserEntity) : Promise<string>{
        return null
    }

    async update(id: number, user : UserUpdateRequest) : Promise<void>{

    }

    async updatePassword(newPassword: string, entity: UserEntity) : Promise<void>{

    }

    async create(user: UserCreateRequest) : Promise<void>{

    }

    async delete(id: number): Promise<void>{

    }
    async findAll(): Promise<UserFindAllResponse>{
        return null
    }
}