import {Module, } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { UserEntity } from '../entities/user.entity';
import { UserRolerEntity } from '../entities/userRole.entity';
import { PermissionsEntity } from '../entities/permissions.entity';
import { UserPermissionsEntity } from '../entities/userPermissions.entity';
import { RolesEntity } from '../entities/roles.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([RolesEntity, UserRolerEntity, UserEntity, PermissionsEntity, UserPermissionsEntity, PermissionsEntity]), 
  ],
  controllers: [UserController],
  providers: [UserService],
})
export class UserModule{
   
}
