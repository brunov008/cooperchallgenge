import { Injectable, HttpException } from '@nestjs/common';
import { InjectRepository, InjectConnection } from '@nestjs/typeorm';
import { Repository, QueryFailedError, Connection } from 'typeorm';
import { LoginRequest } from '../models/login.request.models';
import { UserEntity } from '../entities/user.entity';
import { UserRolerEntity } from '../entities/userRole.entity';
import { LoginRecoverRequest } from '../models/login.recover.request';
import { BaseService } from '../shared/service.shared'
import { LoginRecoverPart2Request } from '../models/login.recover.two.request';
import { Utils } from '../util/utility.util'
import { UserCreateRequest } from '../models/user.request.create.models';
import { UserUpdateRequest } from '../models/user.request.update.models';
import { PermissionsEntity } from '../entities/permissions.entity';
import { UserPermissionsEntity } from '../entities/userPermissions.entity';
import { UserFindAllResponse } from '../models/user.response.findAll.models';
import { RolesEntity } from '../entities/roles.entity';
import { LoginResponse } from '../models/login.response.models';
import { UserFindOneResponse } from '../models/user.response.find';

const sha256 = require('sha256')

@Injectable()
export class UserService implements BaseService{

    constructor(
        @InjectConnection() private connection: Connection,

        @InjectRepository(UserRolerEntity)
        private readonly userRoleRepository: Repository<UserRolerEntity>,

        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>,

        @InjectRepository(PermissionsEntity)
        private readonly permissionsRepository: Repository<PermissionsEntity>,

        @InjectRepository(UserPermissionsEntity)
        private readonly userPermissionsRepository: Repository<UserPermissionsEntity>,

        @InjectRepository(RolesEntity)
        private readonly rolesRepository: Repository<RolesEntity>
      ) {}

      async create({user, password}: UserCreateRequest) : Promise<void> {
        const entity = new UserEntity()
        if(Utils.isCpf(user)){
          entity.cpf = Utils.cleanCpf(user)
        }else if(Utils.isEmail(user)){
          entity.email = user.trim()
        }else{
          throw new HttpException({message: 'Campo user deve conter um cpf ou email válido'}, 400)
        }
        entity.password = sha256(password)

        try{
          //FIXME remodelar tabela banco
          const roles = await this.rolesRepository.find()

          const model = new UserRolerEntity()
          model.id_role = roles.filter(item => item.role === 'Convidado')[0]
          model.id_user = entity

          await this.userRepository.save(entity)
          await this.userRoleRepository.save(model)
          await this.userPermissionsRepository.save({id_user:entity})
        }catch(e){
          if(e instanceof(QueryFailedError) && (e as any).code === '23505'){
            throw new HttpException({message: 'Usuario ja cadastrado'}, 422)
          }
          throw new HttpException({message: 'Ocorreu um erro inesperado'}, 500)
        }
      }

      async update(id: number, {username, cpf, email, password} : UserUpdateRequest) : Promise<void>{
        const _user = await this.userRepository.findOne({id: id})

        if(!_user) throw new HttpException({message: 'Usuario nao cadastrado'}, 404)

        const _result = await this.userRepository.createQueryBuilder()
          .update(_user)
          .set({
            username: username,
            cpf: Utils.cleanCpf(cpf),
            email: email.trim(),
            password: sha256(password)
          })
          .where('id = :id', {id:_user.id})
          .execute()

          if(_result.affected < 1) throw new HttpException({message: 'Ocorreu um erro ao atualizar o usuario'}, 500)
      }

      async delete(id: number): Promise<void> {
        const _user = await this.userRepository.findOne({id: id})

        if(!_user) throw new HttpException({message: 'Usuario nao cadastrado'}, 404)

        try{
          await this.userRoleRepository.delete({ id_user: _user});
          await this.userPermissionsRepository.delete({ id_user: _user});
  
          await this.userRepository.delete({id: id})
        }catch(e){
          throw new HttpException({message: 'Ocorreu um erro ao deletar o usuario'}, 500)
        }
      }

      async updateToken(entity: UserEntity) : Promise<string> {
        const token = Utils.generateCode()

        const _result = await this.userRepository.createQueryBuilder()
          .update(entity)
          .set({token_pass: token})
          .where('id = :id', {id:entity.id})
          .execute()

          if(_result.affected < 1) throw new HttpException({message: 'Ocorreu um erro ao gerar o token no servidor'}, 500)

          return token
      }

      async updatePassword(newPassword: string, entity: UserEntity) : Promise<void>{
        const _result = await this.userRepository.createQueryBuilder()
          .update(entity)
          .set({password: sha256(newPassword), token_pass: null})
          .where('id = :id', {id:entity.id})
          .execute()

          if(_result.affected < 1) throw new HttpException({message: 'Ocorreu um erro ao atualizar a senha'}, 500)

      }

      async recoverByEmailOrCpf({user, token}: LoginRecoverRequest | LoginRecoverPart2Request): Promise<UserRolerEntity> {

        const _result = await this.userRoleRepository
        .createQueryBuilder('usr')
        .innerJoin('usr.id_user', 'id_user')
        .addSelect(['id_user.id', 'id_user.email', 'id_user.created_on', 'id_user.username', 'id_user.password'])
        .where(qb => {
          if(token != null){
            if(Utils.isCpf(user)){
              qb.where('id_user.cpf = :cpf', {cpf: user}).andWhere('id_user.token_pass = :token_passX', {token_passX: token})
            }else if(Utils.isEmail(user)){
              qb.where('id_user.email = :email', {email: user}).andWhere('id_user.token_pass = :token_passY', {token_passY: token})
            }else{
              qb.where('id_user.email = :email', {email: 'bishop3'})
            }
          }else{
            if(Utils.isCpf(user)){
              qb.where('id_user.cpf = :cpf', {cpf: user})
            }else if(Utils.isEmail(user)){
              qb.where('id_user.email = :email', {email: user})
            }else{
              qb.where('id_user.email = :email', {email: 'bishop3'})
            }
          }
        })
        .getOne()

        if (!_result) throw new HttpException({message: 'Usuario nao cadastrado'}, 401)

        return _result
      }

      async findOne({user, password} : LoginRequest): Promise<LoginResponse>{

        const _result = await this.userExistByUserAndPassword(user, password)

        const {id_user, id_role} = _result

        const _permissionsResult = await this.userPermissionsRepository
        .createQueryBuilder('usr')
        .innerJoin('usr.id_user', 'id_user')
        .addSelect(['usr.permissions', 'id_user.id'])
        .where('id_user.id = :id', {id: id_user.id})
        .getOne()

        const response : LoginResponse = {
            id: id_user.id,
            email: id_user.email,
            username: id_user.username,
            permissions: _permissionsResult.permissions,
            role : id_role.role
        }

        return response
      }

      async findAll(): Promise<UserFindAllResponse> {
        
        const _result = await this.userRoleRepository
        .createQueryBuilder('usr')
        .innerJoin('usr.id_user', 'id_user')
        .innerJoin('usr.id_role', 'id_role')
        .addSelect(['id_user.id', 'id_user.cpf','id_user.email', 'id_user.created_on', 'id_user.username', 'id_role.role', 'id_user.master'])
        .getMany()

        const list : UserFindAllResponse = {result : []}

        for await(const item of _result){

          const _permissionsResult = await this.userPermissionsRepository 
          .createQueryBuilder('usr')
          .innerJoin('usr.id_user', 'id_user')
          .addSelect(['usr.permissions', 'id_user.id'])
          .where('id_user.id = :id', {id: item.id_user.id})
          .getOne()

          const ob : UserFindOneResponse= {
            id: item.id,
            email: item.id_user.email,
            cpf:  item.id_user.cpf,
            username: item.id_user.username,
            permissions: _permissionsResult.permissions,
            role: item.id_role.role  
          }

          list.result.push(ob)
        }

        return list
      }

      async userExistByUserAndPassword(user : string, password: string) : Promise<UserRolerEntity>{
        const _result = await this.userRoleRepository
        .createQueryBuilder('usr')
        .innerJoin('usr.id_user', 'id_user')
        .innerJoin('usr.id_role', 'id_role')
        .addSelect(['id_user.id', 'id_user.email', 'id_user.created_on', 'id_user.username', 'id_role.role', 'id_user.master'])
        .where(qb => {
          if(Utils.isCpf(user)){
            qb.where('id_user.cpf = :cpf', {cpf: Utils.cleanCpf(user)}).andWhere('id_user.password = :passwordX', {passwordX: sha256(password)})
          }else if(Utils.isEmail(user)){
            qb.where('id_user.email = :email', {email: user}).andWhere('id_user.password = :passwordY', {passwordY: sha256(password)})
          }
          qb.orWhere('id_user.username = :username', {username: user}).andWhere('id_user.password = :passwordY', {passwordY: sha256(password)})
        })
        .getOne()

        if(!_result) throw new HttpException({message: 'Usuario nao cadastrado'}, 404)

        return _result
      }

}