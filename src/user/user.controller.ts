import { Get, Body, Put, Delete, Param, Controller, Post} from '@nestjs/common';
import { UserService } from './user.service';
import { UserFindAllResponse } from '../models/user.response.findAll.models';
import { BaseResponse } from '../models/base.response';
import { UserUpdateRequest } from '../models/user.request.update.models';
import { ApiOperation, ApiResponse, ApiBody } from '@nestjs/swagger';

@Controller('users')
export class UserController {
    constructor(private userService: UserService) {}

    @ApiOperation({ summary: 'Listar usuários cadastrados' })
    @ApiResponse({ status: 200, description: 'Retorna os usuários.'})
    @ApiResponse({ status: 400, description: 'Retorna uma mensagem contendo erro de validação'})
    @Get()
    async findAll() : Promise<UserFindAllResponse>{ 
        return await this.userService.findAll()
    }

    @ApiOperation({ summary: 'Atualizar informações de um usuário' })
    @ApiResponse({ status: 201, description: 'Retorna uma mensagem de sucesso.'})
    @ApiResponse({ status: 400, description: 'Retorna uma mensagem contendo erro de validação'})
    @ApiResponse({ status: 404})
    @ApiResponse({ status: 500, description: 'Erro no servidor'})
    @Put(':id')
    async update(@Param('id') id : number, @Body() userData: UserUpdateRequest) : Promise<BaseResponse>{
        await this.userService.update(id, userData)

        return { message: 'Usuário atualizado com sucesso.' }
    }

    @ApiOperation({ summary: 'Remover usuário do sistema' })
    @ApiResponse({ status: 200, description: 'Retorna uma mensagem de sucesso.'})
    @ApiResponse({ status: 400, description: 'Erro de validação.'})
    @Delete(':id')
    async delete(@Param('id') id : number) : Promise<BaseResponse>{
        await this.userService.delete(id);

        return { message: 'Usuário excluido com sucesso.' }
    }
} 