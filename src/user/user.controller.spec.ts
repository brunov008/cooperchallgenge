import { Test } from '@nestjs/testing';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { MockService } from '../shared/mock.service.shared';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RolesEntity } from '../entities/roles.entity';
import { UserRolerEntity } from '../entities/userRole.entity';
import { UserEntity } from '../entities/user.entity';
import { PermissionsEntity } from '../entities/permissions.entity';
import { UserPermissionsEntity } from '../entities/userPermissions.entity';
import { INestApplication } from '@nestjs/common';
import { UserUpdateRequest } from 'src/models/user.request.update.models'; 

describe('UserController', () => {
    let app: INestApplication;
    let userController: UserController;

    //ANTES
    beforeAll(async () => {
        const module = await Test.createTestingModule({ 
            imports: [
                TypeOrmModule.forRoot(),
                TypeOrmModule.forFeature([RolesEntity, UserRolerEntity, UserEntity, PermissionsEntity, UserPermissionsEntity, PermissionsEntity]), 
            ],
            controllers: [UserController],
            providers: [UserService],
          })
          //.overrideProvider(UserService) 
          //.useClass(MockService)
          .compile();

          userController = module.get<UserController>(UserController);

          app = module.createNestApplication();
          await app.init();
    });

    //DEPOIS
    afterAll(async () => await app.close());
   
    it('Lista retornada deve conter mais de um elemento', async () => {
        const res = await userController.findAll() //controller

        expect(res.result.length).toBeGreaterThan(0);
    })

    it('Lista retornada deve ser do tipo Array', async () => {
  
        const res = await userController.findAll() //controller

        expect(res.result).toBeInstanceOf(Array);
    })

    it('Lista deve possuir o usuario seed', async () => {
  
        const res = await userController.findAll() //controller

        const result = res.result.some(item => item.username === 'seed')

        expect(result).toBe(true);
    })

    it('Lista deve possuir o usuario seed com o perfil de administrador', async () => {
  
        const res = await userController.findAll() //controller

        const result = res.result.some(item => item.username === 'seed' && item.role === 'ADMIN')

        expect(result).toBe(true);
    })

    it('Os usuarios da lista devem ter pelo menos uma permissao', async () => {
  
        const res = await userController.findAll() //controller

        const result = res.result.some(item => item.permissions.length < 1 )

        expect(result).toBe(false);
    })

    it('Deve atualizar o usuario com o id 3', async () => {

        const request : UserUpdateRequest= {
            cpf: "99999999999",
            email: "bruno",
            password: "1",
            username: "bruno"
        }
  
        const res = await userController.update(3, request) //controller

        const result = { message: 'Usuário atualizado com sucesso.' }

        expect(JSON.stringify(res)).toBe(JSON.stringify(result));
    })
  });