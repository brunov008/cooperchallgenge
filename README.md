# Desafio CooperSystem

Prazo: 5 dias.

## Propósito
Criar um pequeno webservice utilizando o framework Nest.JS

## Requisitos Técnicos Obrigatórios

- NestJs
- TypeORM
- PostgreSQL
- Swagger
- Git
- Rest

## Requisitos Técnicos Opcionais

- Docker
- Teste unitário / integração;
- Teste e2e.
- Seguir o padrão Git Flow

## Requisitos Funcionais
- O usuário deve se registrar com CPF ou E-mail;
- O usuário pode recuperar sua senha através do CPF ou E-mail:
  * Apenas retornar um token que simule o que seria enviado por e-mail;
  * O token deve ser parâmetro para rota alterar a senha.
- O sistema deve possuir os seguintes Perfis pré-cadastrados:
  * Admin
  * Convidado
-  O sistema deve possuir as seguintes Permissões pré-cadastradas:
  * Cadastrar
  * Editar
  * Visualizar
  * Listar
-  O sistema deve possuir um usuário Master default. (Cadastrado como seed);
-  O usuário Master pode vincular Perfil e Permissões aos usuários cadastrados;
-  Desenvolver uma rota, cujo acesso deva ser restrito a um usuário com Perfil e Permissão de sua preferência. A rota não precisa ter implementação, apenas as verificações necessárias para acesso.

## Configuração Necessárias
- Instalação do Node(12.18.0) e NPM (6.14.4)
- Instalação do PostgreSQL(13)

## Rodando a aplicação
- Baixe as dependências com o comando npm i
- Crie uma instância de banco no postgress com as configurações que se encontram no arquivo ormconfig.json
- Execute o script que se encontra na pasta sql para criar as tabelas, colunas e o usuario seed
- Comandos disponíveis
  * npm run start
  * npm run test
- Acesse http://localhost/docs para visualizar a documentação das rotas disponíveis