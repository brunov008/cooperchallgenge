CREATE TABLE public.users
(
    cpf character varying(11) ,
    email character varying(100),
    id serial,
    token_pass text,
    created_on timestamp(6) with time zone DEFAULT CURRENT_TIMESTAMP, 
    last_login timestamp(6) with time zone DEFAULT CURRENT_TIMESTAMP,
    username character varying(50),
    password character varying(254) NOT NULL,
    master bit(1) DEFAULT '0',
    PRIMARY KEY (id),
    UNIQUE(cpf, email, username)
);

CREATE TABLE public.user_roler
(
    id_role integer NOT NULL,
    id_user integer NOT NULL,
    id serial,
    PRIMARY KEY (id)
);

CREATE TABLE public.roles
(
    role character varying(9) NOT NULL,
    id serial,
    created_on timestamp(6) with time zone DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    UNIQUE(role)
);

CREATE TABLE public.permissions
(
    permission character varying(50) NOT NULL,
    created_on timestamp(6) with time zone DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (permission)
);

CREATE TABLE public.user_permissions
(
    id serial,
    permissions character varying [50] DEFAULT '{Visualizar}',
    id_user integer NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE public.user_roler
    ADD FOREIGN KEY (id_role)
    REFERENCES public.roles (id)
    NOT VALID;


ALTER TABLE public.user_roler
    ADD FOREIGN KEY (id_user)
    REFERENCES public.users (id)
    NOT VALID;


ALTER TABLE public.user_permissions
    ADD FOREIGN KEY (id_user)
    REFERENCES public.users (id)
    NOT VALID;

insert into users(cpf, email, username, password, master) VALUES ('99999999999', 'seed@cooper.com', 'seed', '240be518fabd2724ddb6f04eeb1da5967448d7e831c08c8fa822809f74c720a9', '1');
insert into users(cpf, email, username, password) VALUES ('88888888888', 'brunomarcio2006@gmail.com', 'bruno', '240be518fabd2724ddb6f04eeb1da5967448d7e831c08c8fa822809f74c720a9');
insert into users(cpf, email, username, password) VALUES ('11111111111', 'oi@hotmail.com', 'teste', '240be518fabd2724ddb6f04eeb1da5967448d7e831c08c8fa822809f74c720a9');

insert into permissions(permission) values('Cadastrar');
insert into permissions(permission) values('Editar');
insert into permissions(permission) values('Visualizar');
insert into permissions(permission) values('Listar');

insert into roles(role) VALUES ('ADMIN');
insert into roles(role) VALUES ('Convidado');

insert into user_roler(id_user, id_role) values (1,1);
insert into user_roler(id_user, id_role) values (2,2);
insert into user_roler(id_user, id_role) values (3,2);

insert into user_permissions (id_user, permissions) values (1, '{Cadastrar, Visualizar, Editar, Visualizar}');
insert into user_permissions (id_user) values (2);
insert into user_permissions (id_user) values (3);
